---
layout: post
title:  "ENKI server update"
categories: server
---

The ENKI server is now running JupyterLab with a number of extensions that improve server performance. Importantly, the server now has better access to the file system, including a public notebook folder, and has direct access to the ENKI-portal resources at GitLab. Jupyter notebooks work as before, but you may notice that some notebook commands are located in a different part of the user interface. For a tour of the interface, see the YouTube video, [Intro to the ENKI Server](https://www.youtube.com/watch?v=bn_91d-WWzs).

<iframe width="560" height="315" src="https://www.youtube.com/embed/bn_91d-WWzs" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
